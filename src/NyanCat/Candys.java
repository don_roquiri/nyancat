/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NyanCat;

import Backend.ExtendedThread;
import java.awt.geom.Area;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author don_roquiri
 */
public class Candys extends ExtendedThread {

    public JLabel nyanCat;
    public JLabel candys;
    int width;
    int candyScore = 10;
    int score = 0;
    boolean isCandy;
    JFrame scene;
    private int min;
    private int height;

    public Candys(JLabel candy0, JLabel nyanCat, boolean candy, JFrame _scene) {
        super();
        scene = _scene;
        candys = candy0;
        this.nyanCat = nyanCat;
        y = 0;
        isCandy = candy;
        min = candy? 3000 : 600;
        extendedThread.start();
        candys.setVisible(true);
        width = scene.getWidth();
    }

    public synchronized boolean intersects() {
        return new Area(nyanCat.getBounds())
                .intersects(candys.getBounds());
    }
    
    private int y;
    private int sleep;

    @Override
    public void run() {
        while (true) {
            magic();
            try {
                sleep = (int) (Math.random() * 2000) + min;
                extendedThread.sleep(sleep);
                height = scene.getHeight();
                width = scene.getWidth();
                y = (int) (Math.random() *  + height - 20);
                for (int i = width; i >= -25; i--) {
                    extendedThread.sleep(10);
                    candys.setLocation(i, y);
                    if (intersects()) {
                        if (isCandy) {
                            if(Nyan.count == candyScore){
                                min += 300;
                                candyScore += 10;
                            }
                            Nyan.count++;
                            score++;
                        } else {
                            Nyan.count--;
                            if(Nyan.count == -1){
                                JOptionPane.showMessageDialog(null, "Gracias por jugar.\ntu score es: " + score);
                            }
                        }
                        break;
                    }
                }
                candys.setLocation(-35, 0);//Hide
            } catch (InterruptedException ex) {
                Logger.getLogger(Candys.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}