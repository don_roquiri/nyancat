/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NyanCat;

import Backend.ExtendedThread;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Stack;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author don_roquiri
 */
public class Nyan extends JPanel implements KeyListener {

    JLabel nyanCat;
    Candys candys[] = new Candys[6];
    JLabel score;
    ExtendedThread thread;
    boolean status = false; // The game isn't paused
    public static int count;

    private final ImageIcon sprites[] = new ImageIcon[]{
        new ImageIcon(getClass().getResource("/NyanCat/sprites/cat1.png")),
        new ImageIcon(getClass().getResource("/NyanCat/sprites/cat2.png")),
        new ImageIcon(getClass().getResource("/NyanCat/sprites/cat3.png")),
        new ImageIcon(getClass().getResource("/NyanCat/sprites/cat4.png")),
        new ImageIcon(getClass().getResource("/NyanCat/sprites/cat5.png")),
        new ImageIcon(getClass().getResource("/NyanCat/sprites/cat6.png"))
    };

    public Nyan(JLabel nyanCat, JFrame main, JLabel candy0, JLabel candy1, JLabel candy2, JLabel candy3, JLabel candy4, JLabel candy5, JLabel score) {
        super();

        this.nyanCat = nyanCat;
        this.score = score;

        main.addKeyListener(this);

        candys[0] = new Candys(candy0, nyanCat, true, main);
        candys[1] = new Candys(candy1, nyanCat, true, main);
        candys[2] = new Candys(candy2, nyanCat, true, main);
        candys[3] = new Candys(candy3, nyanCat, false, main);
        candys[4] = new Candys(candy4, nyanCat, false, main);
        candys[5] = new Candys(candy5, nyanCat, false, main);

        pX = x = 10;
        pY = y = 120;
        rainbowCoords.add(new Coords(x, y));

        thread = new ExtendedThread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        for (int i = 0; i < 6; i++) {
                            nyanCat.setIcon(sprites[i]);
                            score.setText("" + count);
                            updateLocation();
                            extendedThread.sleep(80);
                            repaint();
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    magic();
                }
            }
        };
        thread.extendedThread.setPriority(Thread.MAX_PRIORITY);
        thread.extendedThread.start();
    }

    int x, y;

    void suspendAll() {
        thread.suspendThread();
        for (int i = 0; i < candys.length; i++) {
            candys[i].suspendThread();
            candys[i].candys.setVisible(false);
        }
    }

    void resumeAll() {
        thread.resumeThread();
        for (int i = 0; i < candys.length; i++) {
            candys[i].resumeThread();
            candys[i].candys.setVisible(true);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    int pX, pY;

    void updateLocation() {
        if (pX != x || pY != y) {
            pX = x;
            pY = y;
            nyanCat.setLocation(x, y);
            rainbowCoords.add(new Coords(x, y));
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (status == false) {
            if (e.getKeyChar() == 's' || e.getKeyChar() == 'S') {
                y += 10;
                updateLocation();
            }
            if (e.getKeyChar() == 'd' || e.getKeyChar() == 'D') {
                x += 10;
                updateLocation();
            }
            if (e.getKeyChar() == 'a' || e.getKeyChar() == 'A') {
                x -= 10;
                updateLocation();
            }
            if (e.getKeyChar() == 'w' || e.getKeyChar() == 'W') {
                y -= 10;
                updateLocation();
            }
        }
        if (e.getKeyChar() == 'x' || e.getKeyChar() == 'X') {
            if (status) {
                System.out.println("Resume");
                resumeAll();
                Sounds.resumeSound();
                status = !status;
            } else {
                System.out.println("Pause");
                Sounds.stopSound();
                suspendAll();
                status = !status;
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    Stack<Coords> rainbowCoords = new Stack<Coords>();

    Color rainbowColors[] = new Color[]{
        Color.red,
        Color.orange,
        Color.yellow,
        Color.green,
        Color.blue,
        new Color(102, 0, 102)
    };

    boolean painting = false;

    private void rainbow(Graphics g) {
        if (!rainbowCoords.isEmpty()) {
            painting = true;
            //Where the cat is located start painting
            for (int i = rainbowCoords.peek().x; i >= 0; i -= 10) {
                //Rainbow colors follows nyan label coords
                spaceBetween = 0;
                for (int j = 0; j < 6; j++) {
                    g.setColor(rainbowColors[j]);
                    if (conditionCount <= 3) {
                        g.fillRect(i,
                                (rainbowCoords.peek().y) + spaceBetween,
                                10, 16);
                    } else if (conditionCount <= 6) {
                        g.fillRect(i,
                                (rainbowCoords.peek().y + spaceBetween + 5),
                                10, 16);
                        if (conditionCount >= 6) {
                            conditionCount = 0;
                        }
                    }
                    spaceBetween += 16;
                }
                conditionCount++;
            }
            if (rainbowCoords.size() > 1) {
                rainbowCoords.remove(0);
            }
            painting = false;
        }
    }
    private byte spaceBetween = 0;
    private byte conditionCount = 0;

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (!painting) {
            rainbow(g);
        }
    }
}

class Coords {

    int x;
    int y;

    public Coords() {
    }

    public Coords(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setLocation(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
