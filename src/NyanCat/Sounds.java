package NyanCat;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Sounds {

    static String root;
    static Clip sound;
    static int frames = 0;

    public Sounds() {
        sound = getSound();
        playSound();
    }

    public Sounds(String root) {
        this.root = root;
        System.out.println(root);
        sound = getSound();
        playSound();
    }

    public Sounds(InputStream in) throws IOException, UnsupportedAudioFileException {
        InputStream bufferedIn = new BufferedInputStream(in);
        audioInputStream = AudioSystem.getAudioInputStream(bufferedIn);
        sound = getSound();
        playSound();
    }
    AudioInputStream audioInputStream;

    public Clip getSound() {
        try {
            if (audioInputStream == null) {
                audioInputStream = AudioSystem.getAudioInputStream(new File(root));
            }
            AudioFormat format = audioInputStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            Clip sound = (Clip) AudioSystem.getLine(info);
            sound.loop(Clip.LOOP_CONTINUOUSLY);
            sound.open(audioInputStream);
            return sound;
        } catch (Exception e) {
            System.out.println("e: " + e);
            return null;
        }
    }

    public void playSound() {
        sound.stop();
        sound.setFramePosition(0);
        sound.start();
    }

    public static void resumeSound() {
        sound.stop();
        sound.setFramePosition(frames);
        sound.start();
    }

    public static void stopSound() {
        frames = sound.getFramePosition();
        sound.stop();
    }
}
