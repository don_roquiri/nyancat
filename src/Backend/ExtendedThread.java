/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Backend;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author don_roquiri
 */
public abstract class ExtendedThread implements Runnable {

    public boolean suspend;
    public boolean stop;
    public Thread extendedThread;

    /**
     * Implement run method using lambda expression
     */
//    Runnable run = () -> {
//        System.out.println("Hello world from a thread");
//    };
//    Thread hilo = new Thread(run);
    /**
     * Per default instance
     */
    public ExtendedThread() {
        super();
        extendedThread = new Thread(this);
        suspend = false;
        stop = false;
    }

    public ExtendedThread(Runnable a) {
        extendedThread = new Thread(a);
        suspend = false;
        stop = false;
    }

    /**
     * Per default instance with set name
     *
     * @param name
     */
    public ExtendedThread(String name) {
        super();
        extendedThread = new Thread(this, name);
        suspend = false;
        stop = false;
    }

    /**
     * If the thread it's suspended stop it
     */
    public synchronized void stophilo() {
        stop = true;
        suspend = false;
        notify();
    }

    /**
     * Suspend a thread
     */
    public synchronized void suspendThread() {
        suspend = true;
    }

    /**
     * resume Thread
     */
    public synchronized void resumeThread() {
        suspend = false;
        notify();
    }

    /**
     * This method need's to be implemented in all run method's
     */
    public synchronized void magic() {
        synchronized (this) {
            while (suspend) {
                try {
                    wait();
                } catch (InterruptedException ex) {
                    Logger.getLogger(ExtendedThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (stop) {
                //Stop
                System.out.println("stop this thread " + extendedThread.getName());
            }
        }
    }

    @Override
    public abstract void run();

}
