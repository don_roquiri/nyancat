/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Files;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;

/**
 *
 * @author don_roquiri
 */
public class ReaderWritter {

    public static void parsingFromJsonToObject(String json) {
        Gson gson = new Gson();
        Player car = gson.fromJson(json, Player.class);
    }

    public static String parsingObjectToJson(Player player) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(player);
    }

    public static boolean doesExist(String path) {
        File archivo = new File(path);
        return archivo.exists();
    }

}
