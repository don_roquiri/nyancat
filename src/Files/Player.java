/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Files;

import java.io.Serializable;

/**
 *
 * @author don_roquiri
 */
public class Player implements Serializable{
    String pathImage;
    String name;
    int number;

    public Player(String pathImage, String name, int number) {
        this.pathImage = pathImage;
        this.name = name;
        this.number = number;
    }
        
}
