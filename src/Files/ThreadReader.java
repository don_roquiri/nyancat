/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Files;

import javax.swing.JOptionPane;

/**
 *
 * @author don_roquiri
 */
public class ThreadReader extends Thread {

    String path = "";

    public ThreadReader(String path) {
        super();
        this.path = path;
    }

    @Override
    public void run() {
        try {
            if (ReaderWritter.doesExist(path)) {
                ReaderWritter.parsingFromJsonToObject(path);
            } else {
                JOptionPane.showMessageDialog(null, "El documento no existe");
                wait();
            }
        } catch (Exception e) {
        }
    }

}
