/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEnd;

import NyanCat.Sounds;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author don_roquiri
 */
public class LoadScreen extends JFrame {

    JProgressBar loadBar = new JProgressBar();
    JLabel icon = new JLabel(new ImageIcon(getClass().getResource("/drawables/question.png")));
    JPanel container = new JPanel(new BorderLayout());
    boolean doesSupportTranslucent = true;

    public LoadScreen() throws HeadlessException {
        initComponents();
        Loader load = new Loader(loadBar, this);
        load.start();
        load = null;
    }

    private void stateChanged0(javax.swing.event.ChangeEvent evt) {
        if (doesSupportTranslucent) { //Does the graphic driver support translucent Java API? make window translucent 
            setOpacity(Float.valueOf((float) loadBar.getValue() / 100f));
        }
        if (loadBar.getValue() >= 100) { //Process it's done             
            System.out.println("100");
            System.out.println(File.separator);
            try {
                new Sounds(getClass().getResourceAsStream("/NyanCat/welcome.wav"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            new Seleccion();
            dispose();
        }
    }

    void initComponents() {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();

        if (!gd.isWindowTranslucencySupported(GraphicsDevice.WindowTranslucency.TRANSLUCENT)) {
            System.err.println("Translucency is not supported");
            doesSupportTranslucent = false;
        }

        //setDefaultLookAndFeelDecorated(true);
        loadBar.setForeground(new Color(0, 255, 0));
        loadBar.setBackground(new Color(255, 255, 255));
        loadBar.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                stateChanged0(e);
            }
        });

        container.setBackground(new Color(102, 0, 102));
        container.add(icon, BorderLayout.CENTER);
        container.add(loadBar, BorderLayout.SOUTH);

        setContentPane(container);

        setUndecorated(true);
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }
}

class Loader extends Thread {

    JProgressBar loadBar;

    public Loader(JProgressBar loadBar, JFrame a) {
        super();
        this.loadBar = loadBar;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i <= 101; i++) {
                loadBar.setValue(i);
                loadBar.setForeground(new Color((int) (Math.random() * 255),
                        (int) (Math.random() * 155 + 100),
                        (int) (Math.random() * 105 + 150)));
                Thread.sleep(40);
            }
        } catch (Exception e) {

        }
    }

}
