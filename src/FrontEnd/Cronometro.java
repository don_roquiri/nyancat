package FrontEnd;

import Backend.ExtendedThread;
import java.awt.Color;
import java.util.StringTokenizer;
import javax.swing.JLabel;
import org.joda.time.DateTime;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author don_roquiri
 */
public class Cronometro extends ExtendedThread {

    JLabel timelbl;
    JLabel dateLbl;
    public int horas, minutos, segundos;
    public int tipo;
    boolean flag = true;
    boolean pausar = false;

    public Cronometro(int tipoDeCronometro, JLabel timelbl) {
        this.timelbl = timelbl;
        tipo = tipoDeCronometro;
        switch (tipoDeCronometro) {
            case 0://Cronometro
                horas = 0;
                minutos = 0;
                segundos = 0;
                break;
            case 1://Reloj
                DateTime dt = new DateTime();
                horas = dt.getHourOfDay();
                minutos = dt.getMinuteOfHour();
                segundos = dt.getSecondOfMinute();
                break;
            case 2: //Temporizador
                horas = 0;
                minutos = 0;
                segundos = 0;
                break;
        }
        extendedThread.start();
        suspendThread();
    }

    public Cronometro(int tipoDeCronometro, JLabel timelbl, JLabel date) {
        this.timelbl = timelbl;
        dateLbl = date;
        tipo = tipoDeCronometro;

        DateTime dt = new DateTime();
        horas = dt.getHourOfDay();
        minutos = dt.getMinuteOfHour();
        segundos = dt.getSecondOfMinute();

        extendedThread.start();
    }

    public void run() {
        while (true) {
            if (pausar) {
                horas = 0;
                minutos = 0;
                segundos = 0;
                timelbl.setText(toString());
            }
            magic();
            if (tipo == 0) {//Cronometro
                try {
                    extendedThread.sleep(1000);
                    segundos++;
                    segundosMinutos();
                    minutosHoras();
                    timelbl.setText(toString());
                } catch (Exception e) {

                }
            } else if (tipo == 1) {//Reloj
                try {
                    extendedThread.sleep(1000);
                    DateTime dt = new DateTime();
                    horas = dt.getHourOfDay();
                    minutos = dt.getMinuteOfHour();
                    segundos = dt.getSecondOfMinute();
                    timelbl.setText(toStringClock());
                    dateLbl.setText(dt.getDayOfMonth() + "/" + dt.getMonthOfYear() + "/" + dt.getYear());

                } catch (Exception e) {
                }
            } else {//Tempo
                try {
                    if (minutos <= 0 && segundos <= 0 && horas <= 0) {
                        timelbl.setForeground(Color.red);
                        extendedThread.sleep(500);
                        timelbl.setForeground(Color.black);
                        extendedThread.sleep(500);
                    } else {
                        extendedThread.sleep(1000);
                        temporizador();
                        System.out.println(toString());
                        timelbl.setText(toString());
                    }
                } catch (Exception e) {
                }

            }
        }
    }

    public synchronized void pausar() {
        pausar = true;
    }

    public synchronized void despausar() {
        pausar = false;
    }

    public synchronized void setTempo(String tempo) {
        StringTokenizer st = new StringTokenizer(tempo, ":");
        horas = Integer.valueOf(st.nextToken());
        minutos = Integer.valueOf(st.nextToken());
        segundos = Integer.valueOf(st.nextToken());
    }

    public synchronized void segundosMinutos() {
        if (segundos >= 60) {
            segundos = 0;
            minutos++;
        }
    }

    public synchronized void minutosHoras() {
        if (minutos >= 60) {
            minutos = 0;
            horas++;
        }
    }

    public synchronized void temporizador() {
        if (segundos > 0) {
            segundos--;
        } else {
            segundos = 59;
            if (minutos > 0) {
                minutos--;
            } else {
                minutos = 59;
            }
            if (horas > 0) {
                horas--;
            }
        }
    }

    @Override
    public synchronized String toString() {
        String ret = "";
        if (horas > 9) {
            ret += horas;
        } else {
            ret += "0" + horas;
        }
        if (minutos > 9) {
            ret += ":" + minutos;
        } else {
            ret += ":" + "0" + minutos;
        }
        if (segundos > 9) {
            ret += ":" + segundos;
        } else {
            ret += ":" + "0" + segundos;
        }
        return ret;
    }

    public synchronized String toStringClock() {
        String ret = "";
        if (horas > 9) {
            ret += horas;
        } else {
            ret += "0" + horas;
        }
        if (minutos > 9) {
            ret += ":" + minutos;
        } else {
            ret += ":" + "0" + minutos;
        }
        return ret;
    }
}
